import csv
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', -1)


# Reading CSVs
def load_un_csv(filename, resultdict={}):
    with open(filename, errors="backslashreplace") as csvfile:
        next(csvfile)
        next(csvfile)
        csvrdr = csv.DictReader(csvfile, fieldnames=('Region/Country/Area', 'Region/Country/Area Name', 'Year',
                                                     'Series', 'Value', 'Footnotes', 'Source'))
        for row in csvrdr:
            loc = (row['Region/Country/Area'], row['Region/Country/Area Name'])
            year = row['Year']
            if loc in resultdict:
                if year in resultdict[loc]:
                    resultdict[loc][year][row['Series']] = row['Value']
                else:
                    resultdict[loc][year] = {}
                    resultdict[loc][year][row['Series']] = row['Value']
            else:
                resultdict[loc] = {}
                resultdict[loc][year] = {}
                resultdict[loc][year][row['Series']] = row['Value']
            if 'Source' not in resultdict[loc][year]:
                resultdict[loc][year]['Source'] = row['Source']
        return resultdict

def train_grade(model):
    model.fit(X_train, y_train)
    # Visualising predicted/real results
    sns.scatterplot(y_test, model.predict(X_test))
    plt.axis('equal')
    plt.ylabel('Tretiary_perc_predict')
    plt.plot([0, 0.08], [0, 0.08], lw=2, color='#414242', linestyle='dashed')
    plt.xlim(0, None)
    plt.ylim(0, None)
    plt.show()
    print(model.score(X_test, y_test))



if __name__ == '__main__':
    # Reading initial CSVs into dict and forming a concatenated dictionary
    edu_dict = load_un_csv('SYB62_309_201906_Education.csv')
    edu_pop_dict = load_un_csv('SYB62_1_201907_Population.csv', edu_dict)
    pd_dict = {}
    cols_to_fill = {'Population mid-year estimates (millions)', 'Sex ratio (males per 100 females)'}
    for loc in edu_pop_dict:
        tempdict = {'Year': [], 'Population mid-year estimates (millions)': [],
                    'Sex ratio (males per 100 females)': [],
                    'Students enrolled in primary education (thousands)': [],
                    'Gross enrollement ratio - Primary (male)': [],
                    'Gross enrollment ratio - Primary (female)': [],
                    'Students enrolled in secondary education (thousands)': [],
                    'Gross enrollment ratio - Secondary (male)': [],
                    'Gross enrollment ratio - Secondary (female)': [],
                    'Students enrolled in tertiary education (thousands)': [],
                    'Gross enrollment ratio - Tertiary (male)': [],
                    'Gross enrollment ratio - Tertiary (female)': []}
        for year in edu_pop_dict[loc]:
            for tkey in tempdict.keys():
                if tkey == 'Year':
                    tempdict[tkey].append(year)
                else:
                    tempval = edu_pop_dict[loc][year].get(tkey, None)
                    if tempval:
                        tempval = float(tempval.replace(',', ''))
                    tempdict[tkey].append(tempval)
        # Creating a temp dataframe to interpolate and collect them into a dict of dataframes by location
        temp_df = pd.DataFrame.from_dict(tempdict)
        temp_df.fillna(value=pd.np.nan, inplace=True)
        for col in cols_to_fill:
            temp_df[col] = temp_df[col].interpolate(limit_area='inside')
            temp_df[col] = temp_df[col].interpolate(limit_area='outside', limit_direction='both')
        temp_df = temp_df.dropna(subset=['Students enrolled in primary education (thousands)',
                                         'Gross enrollement ratio - Primary (male)',
                                         'Gross enrollment ratio - Primary (female)',
                                         'Students enrolled in secondary education (thousands)',
                                         'Gross enrollment ratio - Secondary (male)',
                                         'Gross enrollment ratio - Secondary (female)',
                                         'Students enrolled in tertiary education (thousands)',
                                         'Gross enrollment ratio - Tertiary (male)',
                                         'Gross enrollment ratio - Tertiary (female)',
                                         'Population mid-year estimates (millions)'], how='any')
        if not temp_df.empty:
            pd_dict[loc] = temp_df
    lenf = 0
    frames = []
    # Removing joint locations (like the whole world)
    summarized_locs = ['1', '15', '202', '21', '419', '30', '35', '34', '145', '150', '9']
    for loc in pd_dict:
        if loc[0] in summarized_locs:
            continue
        # Transforming the data and adding dataframes into a list to concat them into a full dataframe
        pd_dict[loc]['Gross enrollment ratio\nSecondary (mean)'] = (pd_dict[loc]['Gross enrollment ratio - Secondary (male)'] + pd_dict[loc]['Gross enrollment ratio - Secondary (female)']) /2
        pd_dict[loc]['Gross enrollment ratio\n- Tertiary (male)'] = pd_dict[loc]['Gross enrollment ratio - Tertiary (male)']
        pd_dict[loc]['Location'] = loc[1]
        pd_dict[loc]['Primary_prop'] = 1 / (pd_dict[loc]['Gross enrollement ratio - Primary (male)'] / pd_dict[loc]['Gross enrollment ratio - Primary (female)'] * 100 / pd_dict[loc]['Sex ratio (males per 100 females)'])
        pd_dict[loc]['Secondary_prop'] = 1 / (pd_dict[loc]['Gross enrollment ratio - Secondary (male)'] / pd_dict[loc]['Gross enrollment ratio - Secondary (female)'] * 100 / pd_dict[loc]['Sex ratio (males per 100 females)'])
        pd_dict[loc]['Tretiary_prop'] = 1 / (pd_dict[loc]['Gross enrollment ratio - Tertiary (male)'] / pd_dict[loc]['Gross enrollment ratio - Tertiary (female)'] * 100 / pd_dict[loc]['Sex ratio (males per 100 females)'])
        pd_dict[loc]['Primary_perc'] = pd_dict[loc]['Students enrolled in primary education (thousands)'] / pd_dict[loc]['Population mid-year estimates (millions)'] / 1000
        pd_dict[loc]['Secondary_perc'] = pd_dict[loc]['Students enrolled in secondary education (thousands)'] / pd_dict[loc]['Population mid-year estimates (millions)'] / 1000
        pd_dict[loc]['Tretiary_perc'] = pd_dict[loc]['Students enrolled in tertiary education (thousands)'] / pd_dict[loc]['Population mid-year estimates (millions)'] / 1000
        pd_dict[loc]['Primary_delta'] = abs(pd_dict[loc]['Primary_prop'] - 1)
        pd_dict[loc]['Secondary_delta'] = abs(pd_dict[loc]['Secondary_prop'] - 1)
        pd_dict[loc]['Tretiary_delta'] = abs(pd_dict[loc]['Tretiary_prop'] - 1)
        pd_dict[loc]['Primary_maxgross'] = pd_dict[loc][['Gross enrollement ratio - Primary (male)', 'Gross enrollment ratio - Primary (female)']].max(axis=1)
        pd_dict[loc]['Secondary_maxgross'] = pd_dict[loc][['Gross enrollment ratio - Secondary (male)', 'Gross enrollment ratio - Secondary (female)']].max(axis=1)
        pd_dict[loc]['Tretiary_maxgross'] = pd_dict[loc][['Gross enrollment ratio - Tertiary (male)', 'Gross enrollment ratio - Tertiary (female)']].max(axis=1)
        frames.append(pd_dict[loc][['Gross enrollment ratio\nSecondary (mean)', 'Secondary_perc', 'Tretiary_perc', 'Tretiary_prop', 'Tretiary_delta', 'Gross enrollment ratio\n- Tertiary (male)']])
        #frames.append(pd_dict[loc])
        #print(loc)
        #print(pd_dict[loc])
        lenf += pd_dict[loc].count()
    fulldf = pd.concat(frames)
    # Printing values
    #print(lenf)
    print(fulldf.corr())
    # After finding out that proportion is more important than diversity we are dropping unneded attribute
    fulldf.pop('Tretiary_delta')
    # Just found outliners
    sns.boxplot(x='Tretiary_prop', data=fulldf)
    plt.show()
    # And got rid of them checking the correlation again
    qmin = fulldf['Tretiary_prop'].quantile(0.05)
    qmax = fulldf['Tretiary_prop'].quantile(0.95)
    newfulldf = fulldf[fulldf['Tretiary_prop'] > qmin]
    newfulldf = newfulldf[newfulldf['Tretiary_prop'] < qmax].reset_index(drop=True)
    sns.boxplot(x='Tretiary_prop', data=newfulldf)
    plt.show()
    print(newfulldf.corr())
    # Was needed for bigger heatmap
    #plt.subplots(figsize=(10,10))
    sns.heatmap(newfulldf.corr())
    plt.show()
    #print(newfulldf)
    # Drawing plots to take a look at data we have (used to adjust some transforming formulas above)
    sns.pairplot(newfulldf)
    plt.show()
    # Preparing dataframe for training and training
    modeldf = newfulldf[['Gross enrollment ratio\nSecondary (mean)', 'Tretiary_prop', 'Tretiary_perc']]
    X = modeldf[['Gross enrollment ratio\nSecondary (mean)', 'Tretiary_prop']].values
    y = modeldf['Tretiary_perc']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    train_grade(linear_model.LinearRegression())
    train_grade(SVR(C=0.4, epsilon=0.0012))





